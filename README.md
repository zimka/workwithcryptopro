# README #

### Intro ###

This repository contains:

* Java classes for working with CryptoPro JCP
* javascript lib for working with CryptoPro Browser Plugin

### javascript ###

Javacript lib is located in directory** src/ru/lanit/ykoroleva/js_files**.

* cryptoProLib.js -- my lib.
* cadesplugin_api.js -- cryptopro lib.
* test.html -- sample.

### Java ###

All Java classes are located in **src/ru/lanit/ykoroleva**. Samples are located in **src/ru/lanit/ykoroleva/samples**. All working classes have documentation in directory **Docs/**. To see all documentation open file **Docs/index.html**