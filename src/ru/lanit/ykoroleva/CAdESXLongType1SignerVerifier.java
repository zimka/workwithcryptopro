package ru.lanit.ykoroleva;

import ru.CryptoPro.CAdES.CAdESSignature;
import ru.CryptoPro.CAdES.CAdESType;
import ru.CryptoPro.CAdES.exception.CAdESException;

import java.io.*;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;

/**
 * @author Yana Koroleva
 */
public class CAdESXLongType1SignerVerifier {

    private CAdESSignature signature;
    private boolean isDetached;

    public CAdESXLongType1SignerVerifier(boolean isDetached) {
        try {
            signature = new CAdESSignature(isDetached);
            this.isDetached = isDetached;
        } catch (CAdESException e) {
            System.out.println("Can't create new CAdES signature.");
            e.printStackTrace();
        }
    }

    public boolean addSigner(String providerName, String digestAlgo, String keyAlgo,
                             PrivateKey privateKey, List<Certificate> chain,
                             String timeServer) {
        try {
            signature.addSigner(providerName, digestAlgo, keyAlgo,
                    privateKey, chain, CAdESType.CAdES_X_Long_Type_1,
                    timeServer, false);
        } catch (CAdESException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void signFile(String path, String input, String output) {
        if (path == null) {
            path = System.getProperty("user.home");
        }

        try (FileOutputStream outputStream =
                     new FileOutputStream(path + File.separator + output)) {
            signature.open(outputStream);
            try (FileInputStream inputStream =
                         new FileInputStream(path + File.separator + input)) {
                int alreadyRead = 0, total = inputStream.available();
                byte[] tmp = new byte[total];

                while (alreadyRead < total) {
                    alreadyRead += inputStream.read(tmp, alreadyRead, total - alreadyRead);
                }
                signature.update(tmp);
            } catch (IOException e) {
                System.out.println("Problems with input file: " + path + File.separator + input);
                e.printStackTrace();
            }
            signature.close();
        } catch (IOException e) {
            System.out.println("Problems with output file: " + path + File.separator + output);
            e.printStackTrace();
        } catch (CAdESException e) {
            System.out.println("Problems with CAdES signature.");
            e.printStackTrace();
        }

    }

    public static void verifyFile(String path, String dataFile, String signFile) {
        try (FileInputStream signStream = new FileInputStream(path + File.separator + signFile)) {
            CAdESSignature decodedSignature = null;
            if (null == dataFile) {
                decodedSignature = new CAdESSignature(signStream, null, null);
            } else {
                try (FileInputStream dataStream = new FileInputStream(path + File.separator + dataFile)) {
                    decodedSignature = new CAdESSignature(signStream, dataStream, null);
                }
                catch (IOException e) {
                    System.out.println("Problems with data file: " + path + File.separator + dataFile);
                    e.printStackTrace();
                }
            }
            if (decodedSignature != null) {
                decodedSignature.verify(Collections.<Certificate>emptySet());
            }
        } catch (IOException e) {
            System.out.println("Problems with signature file: " + path + File.separator + signFile);
            e.printStackTrace();
        } catch (CAdESException e) {
            System.out.println("Problems with decoding CAdES signature.");
            e.printStackTrace();
        }
    }
}
