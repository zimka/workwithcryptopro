package ru.lanit.ykoroleva;

import ru.CryptoPro.JCP.tools.C;
import ru.CryptoPro.JCPRequest.GostCertificateRequest;
import ru.CryptoPro.JCSP.JCSP;

import java.io.ByteArrayInputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

/**
 * This class provides methods for getting different certificates.
 *
 * @author Yana Koroleva
 */
public class CertificateGetter {

    /**
     * This class provides pair of certificate and key pair.
     */
    public static class CertificateKeyPair {
        private Certificate certificate;
        private KeyPair keyPair;

        /**
         * Constuctor of pair.
         *
         * @param certificate   certificate for pair.
         * @param keyPair       key pair for pair.
         */
        public CertificateKeyPair(Certificate certificate, KeyPair keyPair) {
            this.certificate = certificate;
            this.keyPair = keyPair;
        }

        /**
         * Gets certificate from pair.
         *
         * @return certificate.
         */
        public Certificate getCertificate() {
            return this.certificate;
        }

        /**
         * Gets key pair from pair.
         *
         * @return key pair.
         */
        public KeyPair getKeyPair() {
            return this.keyPair;
        }
    }

    /**
     * Gets new certificate from certificate center.
     *
     * @param name                      name of certificate.
     * @param keyGenAlgo                name of algorithm for key pair generating.
     * @param signAlg                   name of algorithm fot signing.
     * @param provider                  name of cryptoprovider.
     * @param certificateCenterAddress  address of certificate center.
     * @return pair of key pair and certificate connected with it.     */
    public static CertificateKeyPair getNewCertificate(String name,
                                                       String keyGenAlgo, String signAlg,
                                                       String provider, String certificateCenterAddress) {
        KeyPairGenerator keyPairGenerator;
        KeyPair keyPair;

        GostCertificateRequest request;

        try {
            keyPairGenerator = KeyPairGenerator.getInstance(keyGenAlgo, provider);
            keyPair = keyPairGenerator.generateKeyPair();

            request = new GostCertificateRequest(JCSP.PROVIDER_NAME);
            request.init(keyGenAlgo);
            request.setSubjectInfo("CN=" + name + ",C=RU");
            request.setPublicKeyInfo(keyPair.getPublic());
            request.encodeAndSign(keyPair.getPrivate(), signAlg);

            byte[] encodedCertificate =
                    request.getEncodedCert(certificateCenterAddress);

            CertificateFactory cf = CertificateFactory.getInstance("X509");
            return new CertificateKeyPair(
                    cf.generateCertificate(new ByteArrayInputStream(encodedCertificate)),
                    keyPair);
        } catch (Exception e) {
            System.out.println("Can't create new certificate.");
            e.printStackTrace();
            return null;
        }
    }
}
