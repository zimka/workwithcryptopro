package ru.lanit.ykoroleva;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

/**
 * This class provides interface fo working with certificate stores.
 *
 * @author Yana Koroleva
 */
public class MyCertificateStore {
    private String path;
    private String name;
    private KeyStore keyStore;
    private String password;

    /**
     * Constructor of certificate store with special path.
     *
     * @param path      path of certificate store.
     * @param name      name of certificate store.
     * @param password  password for certificate store.
     */
    public MyCertificateStore(String path, String name, String password) {
        this.path = path;
        this.name = name;
        this.password = password;
        try {
            keyStore = KeyStore.getInstance("CertStore", "JCP");
        } catch (KeyStoreException | NoSuchProviderException e) {
            System.out.println("Can't create certificate store");
            e.printStackTrace();
        }
    }


    /**
     * Constructor of certificate store in <tt>user.home</tt>
     *
     * @param name      name of certificate store.
     * @param password  password for certificate store.
     */
    public MyCertificateStore(String name, String password) {
        this(System.getProperty("user.home"), name, password);
    }

    private File getFile() {
        return new File(this.path + File.separator + this.name + ".keystore");
    }

    /**
     * Load certificate store.
     *
     * @param alreadyExist  <tt>true</tt> if certificate store
     *                      associated with <tt>name</tt> already exist.
     */
    public void load(boolean alreadyExist) {
        try {
            if (!alreadyExist) {
                this.keyStore.load(null, null);
            } else {
                File file = getFile();
                if (file == null) {
                    System.out.println("Certificate store doesn't exist.\n" +
                            "Path: " + path + ", Name: " + name);
                } else {
                    this.keyStore.load(new FileInputStream(file), password.toCharArray());
                }
            }
        } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
            System.out.println("Can't load certificate store.\n" +
                    "Path: " + path + ", Name: " + name);
            e.printStackTrace();
        }
    }

    /**
     * Get certificate with this alias.
     *
     * @param alias name of certificate
     * @return certificate with this alias, if exists, <tt>null</tt> otherwise.
     */
    public Certificate getCertificate(String alias) {
        try {
            Certificate certificate = keyStore.getCertificate(alias);
            store();
        } catch (KeyStoreException e) {
            System.out.println("Can't load certificate from certificate store.\n" +
                    "Path: " + path + ", Name: " + name);
            e.printStackTrace();
        }
        return null;
    }

    private void store() {
        File file = getFile();
        try {
            keyStore.store(new FileOutputStream(file), password.toCharArray());
        } catch (KeyStoreException | IOException
                | NoSuchAlgorithmException | CertificateException e) {
            System.out.println("Problems with saving certificate store state.");
            e.printStackTrace();
        }
    }

    /**
     * Set certificate with this alias.
     *
     * @param alias         name of certificate
     * @param certificate   certificate to set
     */
    public void setCertificate(String alias, Certificate certificate) {
        try {
            keyStore.setCertificateEntry(alias, certificate);
        } catch (KeyStoreException e) {
            System.out.println("Can't set new certificate.");
            e.printStackTrace();
        }
        store();
    }
}
