package ru.lanit.ykoroleva;

import java.io.IOException;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;

/**
 * @author Yana Koroleva
 */
public class MyKeyStore {
    private KeyStore keyStore;

    public MyKeyStore(String storeType) {
        try {
            keyStore = KeyStore.getInstance(storeType, "JCP");
        } catch (KeyStoreException | NoSuchProviderException e) {
            System.out.println("Can't create key store.");
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            keyStore.load(null, null);
        } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
            System.out.println("Can't load key store");
            e.printStackTrace();
        }
    }

    public void setKey(String name, PrivateKey privateKey, Certificate certificate) {
        try { // TODO: find a bug.
            keyStore.setKeyEntry(name, privateKey, null, new Certificate[] {certificate});
        } catch (KeyStoreException e) {
            System.out.println("Can't set new key entry to key store.");
            e.printStackTrace();
        }
    }

    public Certificate getCertificate(String alias) {
        try {
            return keyStore.getCertificate(alias);
        } catch (KeyStoreException e) {
            System.out.println("There is no certificate with name \""
                    + alias + "\" in the key store");
            e.printStackTrace();
            return null;
        }
    }
}
