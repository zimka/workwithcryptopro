package ru.lanit.ykoroleva;

import java.io.*;
import java.security.*;

/**
 * This class contains methods for signing and verifying different data.
 *
 * @author Yana Koroleva
 */
public class SignerVerifier {

    /**
     * Signs array of bytes with your private key.
     *
     * @param privateKey    private key for signing.
     * @param algoName      name of signing algorithm.
     * @param providerName  name of cryptoprovider.
     * @param data          array of bytes to sign.
     * @return signed array of bytes or <tt>null</tt> if error occurs.
     */
    public static byte[] sign(PrivateKey privateKey, String algoName, String providerName,
                            byte[] data) {
        try {
            Signature sign = Signature.getInstance(algoName, providerName);
            sign.initSign(privateKey);
            sign.update(data);
            return sign.sign();
        } catch (Exception e) {
            System.out.println("Can't create a new sign.");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Signs file with your private key.
     *
     * @param privateKey    private key for signing.
     * @param algoName      name of signing algorithm.
     * @param providerName  name of cryptoprovider.
     * @param path          path of input and output files, if <tt>null</tt> use <tt>user.home</tt>
     * @param inputName     name of file to sign.
     * @param outputName    name of signed file.
     */
    public static void signFile(PrivateKey privateKey, String algoName, String providerName,
                                String path, String inputName, String outputName) {
        if (path == null) {
            path = System.getProperty("user.home");
        }
        byte[] data;
        try (FileInputStream inputStream =
                     new FileInputStream(path + File.separator + inputName)) {
            int fileLength = inputStream.available();
            data = new byte[fileLength];
            int alreadyRead = 0;

            while (alreadyRead < fileLength) {
                alreadyRead += (byte)inputStream.read(data, alreadyRead, fileLength - alreadyRead);
            }
        } catch (IOException e) {
            System.out.println("Problems with file: " +
                    path + File.separator + inputName);
            e.printStackTrace();
            return;
        }
        byte[] outputData = sign(privateKey, algoName, providerName, data);
        if (outputData == null) {
            return;
        }
        try (FileOutputStream outputStream =
                     new FileOutputStream(path + File.separator + outputName)){
            outputStream.write(outputData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Signs string with your ptivate key.
     *
     * @param privateKey    private key for signing.
     * @param algoName      name of signing algorithm.
     * @param providerName  name of cryptoprovider.
     * @param stringData    string to sign.
     * @param charset       charset of the string.
     * @return signed string or <tt>null</tt> if error occurs.
     */
    public String signString(PrivateKey privateKey, String algoName, String providerName,
                       String stringData, String charset) {
        byte[] data;
        String res = null;

        try {
            data = stringData.getBytes(charset);
            byte[] tmp = sign(privateKey, algoName, providerName, data);
            if (tmp != null) {
                res = new String(tmp, charset);
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("Problems with encoding String \""+ stringData +
                    "\" to array of bytes. Charset: " + charset + ".");
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Verifies array of bytes with your public key.
     *
     * @param publicKey     public key for signing.
     * @param algoName      name of verifying algorithm.
     * @param providerName  name of cryptoprovider.
     * @param data          array of bytes to verify.
     * @param signData      signed array of bytes to verify
     *
     * @return <tt>true</tt> if verified, <tt>false</tt> otherwise.
     */
    public static boolean verify(PublicKey publicKey, String algoName, String providerName,
                                 byte[] data, byte[] signData) {
        try {
            Signature sign = Signature.getInstance(algoName, providerName);
            sign.initVerify(publicKey);
            sign.update(data);
            return sign.verify(signData);
        } catch (Exception e) {
            System.out.println("Can't verify a sign.");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Verifies file with your public key.
     *
     * @param publicKey     public key for signing.
     * @param algoName      name of verifying algorithm.
     * @param providerName  name of cryptoprovider.
     * @param path          path of files, if <tt>null</tt> use <tt>user.home</tt>
     * @param file          name of file to verify.
     * @param signFile      name of signed file to verify.
     *
     * @return <tt>true</tt> if verified, <tt>false</tt> otherwise.
     */
    public static boolean verifyFile(PublicKey publicKey, String algoName, String providerName,
                                 String path, String file, String signFile) {
        if (path == null) {
            path = System.getProperty("user.home");
        }

        byte[] data, signData;
        try (FileInputStream inputStream =
                     new FileInputStream(path + File.separator + file)) {
            int fileLength = inputStream.available();
            data = new byte[fileLength];
            int alreadyRead = 0;

            while (alreadyRead < fileLength) {
                data[alreadyRead++] = (byte)inputStream.read();
            }
        } catch (IOException e) {
            System.out.println("Problems with file: " +
                    path + File.separator + file);
            e.printStackTrace();
            return false;
        }

        try (FileInputStream inputStream =
                     new FileInputStream(path + File.separator + signFile)) {
            int fileLength = inputStream.available();
            signData = new byte[fileLength];
            int alreadyRead = 0;

            while (alreadyRead < fileLength) {
                signData[alreadyRead++] = (byte)inputStream.read();
            }
        } catch (IOException e) {
            System.out.println("Problems with file: " +
                    path + File.separator + signFile);
            e.printStackTrace();
            return false;
        }

        return verify(publicKey, algoName, providerName, data, signData);
    }

    /**
     * Verifies signed string.
     *
     * @param publicKey         your public key to check.
     * @param algoName          name of signing algorithm.
     * @param providerName      name of cryptoprovider.
     * @param stringData        string to compare with.
     * @param stringSignData    string to verify.
     * @param charset           charset of strings.
     * @return <tt>true</tt> if verified, <tt>false</tt> otherwise.
     */
    public boolean verifyString(PublicKey publicKey, String algoName, String providerName,
                          String stringData, String stringSignData, String charset) {
        byte[] data;
        byte[] signData;
        try {
            data = stringData.getBytes(charset);
            signData = stringSignData.getBytes(charset);
            return verify(publicKey, algoName, providerName, data, signData);
        } catch (UnsupportedEncodingException e) {
            System.out.println("Problems with encoding String \""+ stringData +
                    "\" to array of bytes. Charset: " + charset + ".");
            e.printStackTrace();
            return false;
        }
    }

}
