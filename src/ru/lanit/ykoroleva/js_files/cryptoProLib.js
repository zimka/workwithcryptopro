function getErrorMessage(e) {
    var err = e.message;
    if (!err) {
        err = e;
    } else if (e.number) {
        err += " (" + e.number + ")";
    }
    return err;
}

/**
*   This function used to sign dataToSign with certSubjectName certificate,
*       using tspService for time stamp in browsers with NPAPI.
*/
function syncSign(dataToSign, certSubjectName, tspService) {
    try {
        var store = cadesplugin.CreateObject("CAPICOM.Store");
    } catch (err) {
        alert("Can't create CAPICOM.Store. Error: " + getErrorMessage(err));
        return;
    }

    store.Open(cadesplugin.CAPICOM_CURRENT_USER_STORE, cadesplugin.CAPICOM_MY_STORE,
    cadesplugin.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

    var certificates = store.Certificates.Find(
            cadesplugin.CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME, certSubjectName);
    if (certificates.Count == 0) {
        alert("Certificate not found: " + certSubjectName);
        return;
    }

    var certificate = certificates.Item(1);
    try {
        var signer = cadesplugin.CreateObject("CAdESCOM.CPSigner");
    } catch (err) {
        alert("Can't create CAdESCOM.CPSigner. Error: " + getErrorMessage(err));
    }

    try {
        var signedData = cadesplugin.CreateObject("CAdESCOM.CadesSignedData");
    } catch (err) {
        alert("Can't create CAdESCOM.CadesSignedData. Error: " + getErrorMessage(err));
    }
    if (dataToSign) {
        signedData.Content = dataToSign;
        signer.Certificate = certificate;
        signer.TSAAddress = tspService;
        try {
            var signedMessage = signedData.SignCades(signer, cadesplugin.CADESCOM_CADES_X_LONG_TYPE_1);
        } catch (err) {
            alert("Can't create signature. Error: " + getErrorMessage(err));
            return;
        }
    }

    store.Close();
    return signedMessage;
}


/**
*   This function used to sign dataToSign with certSubjectName certificate,
*       using tspService for time stamp in browsers without NPAPI.
*/
function asyncSign(dataToSign, certSubjectName, tspService) {
    return new Promise(function(resolve, reject) {
        cadesplugin.async_spawn(function* (args) {
            try {
                var store = yield cadesplugin.CreateObjectAsync("CAPICOM.Store");
                yield store.Open(cadesplugin.CAPICOM_CURRENT_USER_STORE, cadesplugin.CAPICOM_MY_STORE,
                cadesplugin.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

                var certificatesObj = yield store.Certificates;
                var certificates = yield certificatesObj.Find(cadesplugin.CAPICOM_CERTIFICATE_FIND_SUBJECT_NAME,
                                                certSubjectName);

                var cnt = yield certificates.Count;
                if (cnt == 0) {
                    throw("Certificate not found: " + args[0]);
                }

                var certificate = yield certificates.Item(1);
                var signer = yield cadesplugin.CreateObjectAsync("CAdESCOM.CPSigner");
                yield signer.propset_Certificate(certificate);

                var signedData = yield cadesplugin.CreateObjectAsync("CAdESCOM.CadesSignedData");
                yield signedData.propset_Content(dataToSign);

                yield signer.propset_TSAAddress(tspService);

                var signedMessage = yield signedData.SignCades(signer, cadesplugin.CADESCOM_CADES_X_LONG_TYPE_1);
                store.Close();

                args[2](signedMessage);
            } catch (err) {
                 args[3]("Failed to create signature. Error: " + getErrorMessage(err));
            }
        }, certSubjectName, dataToSign, resolve, reject);
    });
}


/**
*   This function used to verify signedMessage in browsers with NPAPI.
*/
function syncVerify(signedMessage) {
    try {
        var signedData = cadesplugin.CreateObject("CAdESCOM.CadesSignedData");
    } catch (err) {
        alert("Can't create CAdESCOM.CadesSignedData. Error: " + getErrorMessage(err));
    }
    try {
        signedData.VerifyCades(signedMessage, cadesplugin.CADESCOM_CADES_X_LONG_TYPE_1);
    } catch (err) {
        alert("Can't verify signature. Error: " + getErrorMessage(err));
        return false;
    }
    return true;
}

/**
*   This function used to verify signedMessage in browsers without NPAPI.
*/
function asyncVerify(signedMessage) {
    return new Promise(function(resolve, reject) {
        cadesplugin.async_spawn(function* (args) {
            try {
                var signedData = yield cadesplugin.CreateObjectAsync("CAdESCOM.CadesSignedData");
                yield signedData.VerifyCades(args[0], cadesplugin.CADESCOM_CADES_X_LONG_TYPE_1, false);
            } catch (err) {
                args[2]("Can't verify signature. Error: " + getErrorMessage(err));
            }
            args[1](true);
        }, signedMessage, resolve, reject);
    });
}