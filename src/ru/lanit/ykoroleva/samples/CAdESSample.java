package ru.lanit.ykoroleva.samples;

import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCSP.JCSP;
import ru.lanit.ykoroleva.CAdESXLongType1SignerVerifier;
import ru.lanit.ykoroleva.CertificateGetter;

import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yana Koroleva
 */
public class CAdESSample {
    public static void main(String[] args) {

        System.setProperty("com.sun.security.enableCRLDP", "true");
        System.setProperty("com.ibm.security.enableCRLDP", "true");

        CertificateGetter.CertificateKeyPair pair = CertificateGetter
                .getNewCertificate("testCAdES", JCP.GOST_EL_DH_NAME
                        , JCP.GOST_EL_SIGN_NAME, JCSP.PROVIDER_NAME,
                        "http://www.cryptopro.ru/certsrv/");
        if (pair == null) {
            return;
        }


        List<Certificate> certList = new ArrayList<>();
        certList.add(pair.getCertificate());
        CAdESXLongType1SignerVerifier signer = new CAdESXLongType1SignerVerifier(false);
        signer.addSigner(JCP.PROVIDER_NAME, JCP.GOST_DIGEST_OID, JCP.GOST_EL_KEY_OID,
                pair.getKeyPair().getPrivate(), certList,
                "http://www.cryptopro.ru/tsp/tsp.srf");

        signer.signFile(null, "test.txt", "test2.txt");
    }
}
