package ru.lanit.ykoroleva.samples;

import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCSP.JCSP;
import ru.lanit.ykoroleva.CertificateGetter;
import ru.lanit.ykoroleva.MyCertificateStore;

import java.security.cert.Certificate;

/**
 * @author Yana Koroleva
 */
public class MyCertificateStoreSample {
    public static void main(String[] args) {
        MyCertificateStore myCertificateStore = new MyCertificateStore(
                "D:\\Users\\ykoroleva\\Documents", "Test", "1234567890");
        myCertificateStore.load(false);

        CertificateGetter.CertificateKeyPair pair = CertificateGetter
                .getNewCertificate("test1", JCP.GOST_EL_DH_NAME
                        , JCP.GOST_EL_SIGN_NAME, JCSP.PROVIDER_NAME,
                        "http://www.cryptopro.ru/certsrv/");
        if (pair == null) {
            return;
        }

        myCertificateStore.setCertificate("test1", pair.getCertificate());
        Certificate certificate1 = myCertificateStore.getCertificate("test1");
        System.out.println("Cert: " + certificate1.toString());
    }
}
