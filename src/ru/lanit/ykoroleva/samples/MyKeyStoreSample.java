package ru.lanit.ykoroleva.samples;

import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCSP.JCSP;
import ru.lanit.ykoroleva.CertificateGetter;
import ru.lanit.ykoroleva.MyKeyStore;

import java.security.cert.Certificate;

/**
 * @author Yana Koroleva
 */
public class MyKeyStoreSample {
    public static void main(String[] args) {
        MyKeyStore myKeyStore = new MyKeyStore("J6CFStore");
        myKeyStore.load();

        CertificateGetter.CertificateKeyPair pair = CertificateGetter.getNewCertificate("test1",
                JCP.GOST_EL_DH_NAME, JCP.GOST_EL_SIGN_NAME, JCSP.PROVIDER_NAME, "http://www.cryptopro.ru/certsrv/");
        if (pair == null) {
            return;
        }

        myKeyStore.setKey("test1", pair.getKeyPair().getPrivate(), pair.getCertificate()); // TODO: find a bug.
        Certificate certificate1 = myKeyStore.getCertificate("test1");
        System.out.println("Cert1: " + certificate1.toString());
    }
}
