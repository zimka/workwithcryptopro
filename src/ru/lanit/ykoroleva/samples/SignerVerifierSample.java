package ru.lanit.ykoroleva.samples;

import ru.CryptoPro.JCP.JCP;
import ru.CryptoPro.JCSP.JCSP;
import ru.lanit.ykoroleva.SignerVerifier;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * @author Yana Koroleva
 */
public class SignerVerifierSample {
    public static void main(String[] args) {
        KeyPairGenerator keyPairGenerator = null;
        KeyPair keyPair = null;
        try {
            keyPairGenerator = KeyPairGenerator
                    .getInstance(JCP.GOST_EL_DH_NAME, JCSP.PROVIDER_NAME);
            keyPair = keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
        }
        if (keyPair == null) {
            return;
        }
        SignerVerifier.signFile(keyPair.getPrivate(),  JCP.CRYPTOPRO_SIGN_NAME,
                JCSP.PROVIDER_NAME, null, "test.txt", "test2.txt");
        System.out.println(SignerVerifier.verifyFile(keyPair.getPublic(),
                JCP.CRYPTOPRO_SIGN_NAME, JCSP.PROVIDER_NAME, null, "test.txt", "test2.txt"));
    }
}
